﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenerateEncryptFile
{
    public class Message_Collection
    {
        public string save_success = "Save Success!";
        public string save_failed = "Save Failed!";
        public string caption = "Service Config";
        public string timer_not_set = "Time has never been set.";
        public string service_running = "Service Is Running.";
        public string service_pause = "Service Puase.";
        public string service_stoped = "Service Has Stopped.";
        public string default_time = "00:00:00";
        public string service_name = "ERP Data Download";
        public string service_not_available = "Service Not Available.";
    }
}
