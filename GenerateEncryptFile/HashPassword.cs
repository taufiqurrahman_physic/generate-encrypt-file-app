﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace GenerateEncryptFile
{
   public class HashPassword
    {
        public static string HashingTextInput(string input)
        {
            byte[] salt = GetSalt();
            
            using (Rfc2898DeriveBytes hashing = new Rfc2898DeriveBytes(input, salt, 292929))
            {
                byte[] hash = hashing.GetBytes(20);
                byte[] hashPassword = SetHashPassword(salt,hash);

                return Convert.ToBase64String(hashPassword);
            }
        }
        public static int ComparePassword(string dbPassword,string newPassword)
        {
            int key = 1;
            byte[] hashDb = Convert.FromBase64String(dbPassword);
            byte[] salt = new byte[16];
            Array.Copy(hashDb, 0, salt, 0, 16);
            using (Rfc2898DeriveBytes hashing = new Rfc2898DeriveBytes(newPassword, salt, 292929))
            {
                byte[] hash = hashing.GetBytes(20);
                for(int i = 0; i < 20; i++)
                {
                    if (hashDb[i + 16] != hash[i])
                    {
                        key = 0;
                    }
                }
            }

            return key;
        }
        private static byte[] SetHashPassword(byte[] salt,byte[] hash)
        {
            byte[] result = new byte[36];

            
            Array.Copy(salt, 0, result, 0, 16);
            Array.Copy(hash, 0, result, 16, 20);
            return result;
        }

        private static byte[] GetSalt()
        {
            byte[] salt = new byte[16]; // 32 Bytes will give us 256 bits.

           using (RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider())
           {
               // Fill the array with cryptographically secure random bytes.
               rngCsp.GetBytes(salt);
           }
           return salt;
           
        }
    }
}
