﻿namespace GenerateEncryptFile
{
    partial class EncryptorApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EncryptorApp));
            this.EncrytpBtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ConStringTxBx = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.WSDLNameTxBx = new System.Windows.Forms.TextBox();
            this.WSDLPasswordTxBx = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.AdminNameTxbx = new System.Windows.Forms.TextBox();
            this.AdminPasswordTxBx = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.DecryptFileLsBx = new System.Windows.Forms.ListBox();
            this.ClearBtn = new System.Windows.Forms.Button();
            this.DecryptBtn = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.exe_textBox = new System.Windows.Forms.TextBox();
            this.xmlFile_textBox = new System.Windows.Forms.TextBox();
            this.ConStringLbl = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.dtPeriod = new System.Windows.Forms.DateTimePicker();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.LabelServiceStatus = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.ConStringLbl.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // EncrytpBtn
            // 
            resources.ApplyResources(this.EncrytpBtn, "EncrytpBtn");
            this.EncrytpBtn.Name = "EncrytpBtn";
            this.EncrytpBtn.UseVisualStyleBackColor = true;
            this.EncrytpBtn.Click += new System.EventHandler(this.EncrytpBtn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ConStringTxBx);
            this.groupBox1.Controls.Add(this.label4);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // ConStringTxBx
            // 
            resources.ApplyResources(this.ConStringTxBx, "ConStringTxBx");
            this.ConStringTxBx.Name = "ConStringTxBx";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.WSDLNameTxBx);
            this.groupBox3.Controls.Add(this.WSDLPasswordTxBx);
            resources.ApplyResources(this.groupBox3, "groupBox3");
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.TabStop = false;
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // WSDLNameTxBx
            // 
            resources.ApplyResources(this.WSDLNameTxBx, "WSDLNameTxBx");
            this.WSDLNameTxBx.Name = "WSDLNameTxBx";
            // 
            // WSDLPasswordTxBx
            // 
            resources.ApplyResources(this.WSDLPasswordTxBx, "WSDLPasswordTxBx");
            this.WSDLPasswordTxBx.Name = "WSDLPasswordTxBx";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.AdminNameTxbx);
            this.groupBox2.Controls.Add(this.AdminPasswordTxBx);
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // AdminNameTxbx
            // 
            resources.ApplyResources(this.AdminNameTxbx, "AdminNameTxbx");
            this.AdminNameTxbx.Name = "AdminNameTxbx";
            // 
            // AdminPasswordTxBx
            // 
            resources.ApplyResources(this.AdminPasswordTxBx, "AdminPasswordTxBx");
            this.AdminPasswordTxBx.Name = "AdminPasswordTxBx";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.richTextBox1);
            this.groupBox4.Controls.Add(this.DecryptFileLsBx);
            this.groupBox4.Controls.Add(this.ClearBtn);
            this.groupBox4.Controls.Add(this.DecryptBtn);
            resources.ApplyResources(this.groupBox4, "groupBox4");
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.TabStop = false;
            // 
            // richTextBox1
            // 
            resources.ApplyResources(this.richTextBox1, "richTextBox1");
            this.richTextBox1.Name = "richTextBox1";
            // 
            // DecryptFileLsBx
            // 
            this.DecryptFileLsBx.FormattingEnabled = true;
            resources.ApplyResources(this.DecryptFileLsBx, "DecryptFileLsBx");
            this.DecryptFileLsBx.Name = "DecryptFileLsBx";
            // 
            // ClearBtn
            // 
            resources.ApplyResources(this.ClearBtn, "ClearBtn");
            this.ClearBtn.Name = "ClearBtn";
            this.ClearBtn.UseVisualStyleBackColor = true;
            this.ClearBtn.Click += new System.EventHandler(this.ClearBtn_Click);
            // 
            // DecryptBtn
            // 
            resources.ApplyResources(this.DecryptBtn, "DecryptBtn");
            this.DecryptBtn.Name = "DecryptBtn";
            this.DecryptBtn.UseVisualStyleBackColor = true;
            this.DecryptBtn.Click += new System.EventHandler(this.DecryptBtn_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Controls.Add(this.exe_textBox);
            this.groupBox5.Controls.Add(this.xmlFile_textBox);
            resources.ApplyResources(this.groupBox5, "groupBox5");
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.TabStop = false;
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // exe_textBox
            // 
            resources.ApplyResources(this.exe_textBox, "exe_textBox");
            this.exe_textBox.Name = "exe_textBox";
            // 
            // xmlFile_textBox
            // 
            resources.ApplyResources(this.xmlFile_textBox, "xmlFile_textBox");
            this.xmlFile_textBox.Name = "xmlFile_textBox";
            // 
            // ConStringLbl
            // 
            this.ConStringLbl.Controls.Add(this.groupBox5);
            this.ConStringLbl.Controls.Add(this.groupBox4);
            this.ConStringLbl.Controls.Add(this.groupBox2);
            this.ConStringLbl.Controls.Add(this.groupBox3);
            this.ConStringLbl.Controls.Add(this.groupBox1);
            this.ConStringLbl.Controls.Add(this.EncrytpBtn);
            resources.ApplyResources(this.ConStringLbl, "ConStringLbl");
            this.ConStringLbl.Name = "ConStringLbl";
            this.ConStringLbl.TabStop = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            resources.ApplyResources(this.tabControl1, "tabControl1");
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.ConStringLbl);
            resources.ApplyResources(this.tabPage1, "tabPage1");
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox6);
            resources.ApplyResources(this.tabPage2, "tabPage2");
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.dtPeriod);
            this.groupBox6.Controls.Add(this.btnRefresh);
            this.groupBox6.Controls.Add(this.btnSave);
            this.groupBox6.Controls.Add(this.LabelServiceStatus);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Controls.Add(this.label3);
            resources.ApplyResources(this.groupBox6, "groupBox6");
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.TabStop = false;
            // 
            // dtPeriod
            // 
            this.dtPeriod.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            resources.ApplyResources(this.dtPeriod, "dtPeriod");
            this.dtPeriod.Name = "dtPeriod";
            this.dtPeriod.ShowUpDown = true;
            // 
            // btnRefresh
            // 
            resources.ApplyResources(this.btnRefresh, "btnRefresh");
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnSave
            // 
            resources.ApplyResources(this.btnSave, "btnSave");
            this.btnSave.Name = "btnSave";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // LabelServiceStatus
            // 
            resources.ApplyResources(this.LabelServiceStatus, "LabelServiceStatus");
            this.LabelServiceStatus.Name = "LabelServiceStatus";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // EncryptorApp
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "EncryptorApp";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EncryptorApp_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ConStringLbl.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button EncrytpBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox ConStringTxBx;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox WSDLNameTxBx;
        private System.Windows.Forms.TextBox WSDLPasswordTxBx;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox AdminNameTxbx;
        private System.Windows.Forms.TextBox AdminPasswordTxBx;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.ListBox DecryptFileLsBx;
        private System.Windows.Forms.Button ClearBtn;
        private System.Windows.Forms.Button DecryptBtn;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox exe_textBox;
        private System.Windows.Forms.TextBox xmlFile_textBox;
        private System.Windows.Forms.GroupBox ConStringLbl;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label LabelServiceStatus;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DateTimePicker dtPeriod;
    }
}

