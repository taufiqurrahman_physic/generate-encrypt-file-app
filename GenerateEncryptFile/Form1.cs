﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GenerateEncryptFile
{
    public partial class EncryptorApp : Form
    {
        #region Constructor
        //  private string password;
        readonly Path_Directory path = new Path_Directory();
        readonly Message_Collection msg = new Message_Collection();
        public EncryptorApp()
        {
            //  _crp = new CryptoServices();
            InitializeComponent();
            InitDataIfExist();
        }
        #endregion
        #region CheckFileExist
            private void InitDataIfExist()
        {
            if (CryptoServices.CheckIfAppSettingExist())
            {
                string content = CryptoServices.DecryptFromFile();
                if (!string.IsNullOrEmpty(content))
                {
                    var listContent = content.Split('?');
                    if (listContent.Length == 5)
                    {
                        var wsdlList = listContent[1].Split(';');
                        var adminList = listContent[2].Split(';');
                        if (wsdlList.Length == 2 && adminList.Length == 2)
                        {
                            AdminNameTxbx.Text=adminList[0];
                            AdminPasswordTxBx.Text = adminList[1];
                            ConStringTxBx.Text = listContent[0];
                            //EncryptTextTxbx.Text = string.Empty;
                            WSDLNameTxBx.Text = wsdlList[0];
                            WSDLPasswordTxBx.Text = wsdlList[1];
                            xmlFile_textBox.Text = listContent[3];
                            exe_textBox.Text = listContent[4];
                        }
                    }
                }
            }
        }
        #endregion
        #region Encryptor

        private void EncrytpBtn_Click(object sender, EventArgs e)
        {

            string ConnectionString = ConStringTxBx.Text;
            string UserNameWSDL = WSDLNameTxBx.Text;
            string PasswordWSDL = WSDLPasswordTxBx.Text;
            string AdminUserName = AdminNameTxbx.Text;
            string AdminPassword = AdminPasswordTxBx.Text;
            string XMLPath = xmlFile_textBox.Text;
            string ExeFile = exe_textBox.Text;
            if (!string.IsNullOrEmpty(ConnectionString) &&
                !string.IsNullOrEmpty(UserNameWSDL) &&
                !string.IsNullOrEmpty(PasswordWSDL) &&
                !string.IsNullOrEmpty(AdminUserName) &&
                !string.IsNullOrEmpty(AdminPassword) &&
                !string.IsNullOrEmpty(XMLPath) &&
                !string.IsNullOrEmpty(ExeFile))
            {
                string textToEncrypt = ConnectionString + "?"
                                       + UserNameWSDL + ";" + PasswordWSDL + "?"
                                       + AdminUserName + ";" + AdminPassword + "?" + XMLPath + "?" + ExeFile;
                string result = CryptoServices.Encrypt(textToEncrypt);
                if (!string.IsNullOrEmpty(result))
                {
                    //  EncryptTextTxbx.Text = result;
                    richTextBox1.Text = result;
                }
            }
            else
            {
                MessageBox.Show("You Must Insert All Value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void DecryptBtn_Click(object sender, EventArgs e)
        {
            string decrypFromFile = CryptoServices.DecryptFromFile();
            if (!string.IsNullOrEmpty(decrypFromFile))
            {
                var output = decrypFromFile.Split('?');
                string conString = "Connection String    : " + output[0];
                var WSDLAccount = output[1].Split(';');
                string WsdlName = "User Name ( WSDL )   : " + WSDLAccount[0];
                string WsdlPassword = "Password ( WSDL )    : " + WSDLAccount[1];
                var AdminAccount = output[2].Split(';');
                string adminName = "User Name ( Admin )  : " + AdminAccount[0];
                string adminPassword = "Password             : " + AdminAccount[1];
                string xmlFile = "XML Path             : " + output[3];
                string ExeFile = "Exe Path             : " + output[4];
                DecryptFileLsBx.Items.AddRange(new object[]
                {
                    conString,
                    WsdlName,
                    WsdlPassword,
                    adminName,
                    adminPassword,
                    xmlFile,
                    ExeFile
                });
            }

        }
        private string ComputeSha256Hash(string input)
        {
            using (SHA256 sha = SHA256.Create())
            {
                byte[] outputComputing = sha.ComputeHash(Encoding.UTF8.GetBytes(input));

                StringBuilder builder = new StringBuilder();
                foreach (var it in outputComputing)
                {
                    builder.Append(it.ToString("x2"));
                }
                return builder.ToString();
            }

        }
        private void ClearBtn_Click(object sender, EventArgs e)
        {
            AdminNameTxbx.Clear();
            AdminPasswordTxBx.Clear();
            ConStringTxBx.Clear();
            //EncryptTextTxbx.Text = string.Empty;
            WSDLNameTxBx.Clear();
            WSDLPasswordTxBx.Clear();
            DecryptFileLsBx.Items.Clear();
            richTextBox1.Clear();
            xmlFile_textBox.Clear();
            exe_textBox.Clear();
        }
        #endregion

        #region Service Configuration

        #region Methods
        private void SaveData()
        {
            try
            {
                string data = CryptoServices.Encrypt2(dtPeriod.Text);
                StreamWriter data_write = new StreamWriter(path.data_path);
                data_write.Write(data);
                data_write.Close();
                MessageBox.Show(msg.save_success, msg.caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show(msg.save_failed, msg.caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;
            }
        }
        private void GetServiceStatus()
        {
            ServiceController service = new ServiceController(msg.service_name);
            try
            {
                switch (service.Status)
                {
                    case ServiceControllerStatus.Running:
                        {
                            LabelServiceStatus.ForeColor = Color.DarkGreen;
                            LabelServiceStatus.BackColor = Color.LightGreen;
                            LabelServiceStatus.Text = msg.service_running;
                            break;
                        }
                    case ServiceControllerStatus.Paused:
                        {
                            LabelServiceStatus.ForeColor = Color.Yellow;
                            LabelServiceStatus.BackColor = Color.LightYellow;
                            LabelServiceStatus.Text = msg.service_pause;
                            break;
                        }
                    case ServiceControllerStatus.Stopped:
                    default:
                        {
                            LabelServiceStatus.ForeColor = Color.DarkRed;
                            LabelServiceStatus.BackColor = Color.OrangeRed;
                            LabelServiceStatus.Text = msg.service_stoped;
                            break;
                        }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("'" + msg.service_name + "' " + msg.service_not_available, msg.caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
        }
        private void LoadTime()
        {
            if (File.Exists(path.data_path) == true && new FileInfo(path.data_path).Length != 0)
                dtPeriod.Value = Convert.ToDateTime(CryptoServices.Decrypt(File.ReadAllText(path.data_path)));
            else
            {
                MessageBox.Show(msg.timer_not_set, msg.caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                dtPeriod.Value = Convert.ToDateTime(msg.default_time);
            }
        }
        #endregion

        #region Designer Event
        private void Form1_Load(object sender, EventArgs e)
        {
           // GetServiceStatus();
            LoadTime();
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveData();
        }
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            GetServiceStatus();
        }
        private void EncryptorApp_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
        #endregion

        #endregion
    }
}
